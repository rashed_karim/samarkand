
/*
*   Author: Rashed Karim, PhD
*   EY AI Centre of Enablement 
* 
*   Javascript for preparing the loaded raw Python data and plot with ChartJS
*
*/

var plot_data=[]; 
var point_size = 8;
num_clusters = 0; 



for (var i=0;i<cluster_data.length;i++)
{
    // determine max label id 
    var label = Number(cluster_data[i].label);
    if (label > num_clusters)
    {
        num_clusters = label; 
    }
        
}

// initiaise plot data 
for (var i=0;i<1+num_clusters;i++) { 
    plot_data[i] = new Array()
}

// Now re-iterate the cluster data to populate chart data 
for (var i=0;i<cluster_data.length;i++)
{
    var label = cluster_data[i].label;
    var data_point = {}
    data_point.x =  parseFloat(cluster_data[i].x);
    data_point.y = parseFloat(cluster_data[i].y)
    data_point.r = Number(point_size)

    plot_data[label].push(data_point)
}

datasets_to_plot = new Array();
plot_property = {
    label: 'CMB customer clusters',
    backgroundColor: window.chartColors.red,
    borderColor: window.chartColors.red,
    type: 'line',
    pointRadius: 0,
    fill: false,
    lineTension: 0,
    borderWidth: 2
}; 
datasets_to_plot.push(plot_property);

// Randomise colours for the scatter
color_mix = [0, 10, 50, 100, 125, 255];         

cluster_names = ['Large-corp', 'Biz-Upper', 'Biz-Mass', 'Mid-Market','Large-corp'];

for (var i=0;i<plot_data.length;i++)
{
    var dataset_i = {}

    // choose a random rgb color mix
    var color = random_rgba();

    dataset_i.label = cluster_names[Math.floor(Math.random() * cluster_names.length)]; 
    dataset_i.data = plot_data[i];
    dataset_i.borderColor = color;
    dataset_i.borderWidth = '1'; 
    dataset_i.backgroundColor = color;
    dataset_i.type = 'bubble';
    datasets_to_plot.push(dataset_i);
}

// fixed hard-coded annotations for indicating cluster
// remove this if dataset changes 
// specifically for /data/cluster_data_0053_01072019.csv
var dataset_mock_annotation = {}; 
dataset_mock_annotation.label = "Anomalous cluster";
dataset_mock_annotation.data = [
    {'x':25, 'y':3, 'r':48},
    {'x':28, 'y':15, 'r':48},
    {'x':4, 'y':25, 'r':20},
    {'x':5.5, 'y':15, 'r':20},

];
dataset_mock_annotation.borderWidth = 4
dataset_mock_annotation.borderColor = 'rgba(0.4,0.3,1,1)'; 
dataset_mock_annotation.type = 'bubble'; 
dataset_mock_annotation.backgroundColor = 'rgba(0, 0, 0, 0.1)'; 
//dataset_mock_annotation.pointStyle = 'rectRounded';
datasets_to_plot.push(dataset_mock_annotation)


console.log(plot_data)
var config = {
    type: 'bar',
    data: {
        datasets: []
    }, 
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'Unsupervised Learning for Real-time Detection', 
            fontSize: 24
        },
        tooltips: {
            /* Refer to: https://stackoverflow.com/questions/28568773/javascript-chart-js-custom-data-formatting-to-display-on-tooltip */
            callbacks: {
                title: function(tooltipItem, data) {
                    return "Cluster-info";
                }, 
                label: function(tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex].label;
                }
            }
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom'
            }]
        }
    }

   

    
}; // end config 

config.data.datasets = datasets_to_plot;

var ctx = document.getElementById('canvas').getContext('2d');
chart = new Chart(ctx, config);
window.myLine = chart


/**********************************************
*       Functions here
*
***********************************************/ 

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
}
