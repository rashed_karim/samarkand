/*
*   Author: Rashed Karim, PhD 
*   EY AI Centre of Enablement 
*   
*   Javascript for preparing the loaded raw Python data and plot with ChartJS
*/

console.log(timeseries_data);
N = timeseries_data.length
charges = []; 
balance = []; 
mischarges = []; 

// extract time and charge from csv data 
// 
for (var i = 1; i < N; i++) {
    var day_i = timeseries_data[i].day; 
    var parts = day_i.split('/');
    var day_datetime = new Date(parts[2], parts[1], parts[0]); 
    timeseries_label.push(day_datetime);
    
    var charge_i = timeseries_data[i].charge; 
    var timepoint = {'t': day_datetime, 'y':charge_i};

    if (timeseries_data[i].mischarge == 1) {
        var mischarge_timepoint = {'x': day_datetime, 'y': charge_i, 'r': 8}; 
        mischarges.push(mischarge_timepoint);
    }
    charges.push(timepoint); 

}

// extract time and balance from csv data 
for (var i = 1; i < N; i++) {

    var day_i = balance_data[i].day; 
    var parts = day_i.split('/');
    var day_datetime = new Date(parts[2], parts[1], parts[0]); 
    //balance_label.push(day_datetime);
    
    var balance_i = -1 * Number(balance_data[i].balance); 
    var timepoint = {'t': day_datetime, 'y':balance_i}
    balance.push(timepoint); 
}

var MONTHS = timeseries_label;
var config = {
    type: 'bar',
    data: {
        labels: timeseries_label,
        datasets: [
            { 

                label: 'balance', 
                data: balance, 
                type: 'bar', 
                yAxisID: 'y_balance'

            }
            ,
            {
                // overdraft charges data 
                label: 'Daily charges',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: charges,
                type: 'line',
                yAxisID: 'y_charges', 
                pointRadius: 0,
                fill: false,
                lineTension: 0,
                borderWidth: 2
            }, 
            {
                // AI warnings 
                label: 'AI warnings',
                data: mischarges, 
            
                borderColor: 'rgb(0.5, 0, 0)', 
                borderWidth: '2', 
                backgroundColor: 'rgb(255, 255, 0)',
            
                // Bubble for annotating 
                type: 'bubble'
    
            }
        ]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'Anomaly Detection in Customer Charges', 
            fontSize: 24
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                id: 'B',
                type: 'time',
                distribution: 'series',
                ticks: {
                    source: 'data',
                    autoSkip: true, 
                    maxTicksLimit: 12
                },
                time: {
                    unit: 'month'
                }
            }],
            yAxes: [
                {
                    id: 'y_charges', 
                    position: 'left', 
                    scaleLabel: {
                        display: true,
                        labelString: 'Product Charges (£)'
                    }, 
                    ticks: {
                        suggestedMin: 0, 
                        max: 10
                    }
                }, 
                {
                    id: 'y_balance', 
                    position: 'right',
                    scaleLabel: {
                        display: true,
                        labelString: 'Overdraft amount (£)', 
                        
                    }, 
                    ticks: {
                        suggestedMin: 0, 
                        max: 50000
                    }
                }
            ]
        }, 
        tooltips: {
            /* Refer to: https://stackoverflow.com/questions/28568773/javascript-chart-js-custom-data-formatting-to-display-on-tooltip */
            callbacks: {
                title: function(tooltipItem, data) {
                    return "Detected SDIR at "+getRandomSDIRRate()+"%";
                }, 
                label: function(tooltipItem, data) {
                    return "confidence="+Math.round(getRandomArbitrary(0.6,1) * 100) / 100;
                }
            }
        },
        annotation: {
            drawTime: 'beforeDatasetsDraw',
            events: ['dblclick'],
           
            annotations: [
                /*{
                    type: 'line',
                    mode: 'horizontal',
                    scaleID: 'y_charges',
                    value: 1.4,
                    backgroundColor: 'rgba(101, 33, 171, 0.5)',
                    borderColor: 'rgb(101, 33, 171)',
                    borderWidth: 2,
                    onDblclick: function(e) {
                        console.log('Box', e.type, this);
                    }, 
                    
                
                }, 
                {
                    type: 'line',
                    mode: 'horizontal',
                    scaleID: 'y_charges',
                    value: 0.5,
                    backgroundColor: 'rgba(101, 33, 171, 0.5)',
                    borderColor: 'rgb(101, 33, 171)',
                    borderWidth: 2,

                    onDblclick: function(e) {
                        console.log('Box', e.type, this);
                    }, 
                    
                }*/
        
            ]
            
        }
    }

    
}; // end config 


var ctx = document.getElementById('canvas').getContext('2d');

// clear any charts before creating new ones 
// https://stackoverflow.com/questions/40056555/destroy-chart-js-bar-graph-to-redraw-other-graph-in-same-canvas
if (empty_chart != null)
{
    empty_chart.destroy();
}

if (chart != null)
{
    chart.destroy(); 
}
chart = new Chart(ctx, config);



// Functions 
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}


function getRandomSDIRRate() { 
    sdir_mischarge = [0.195]; 
    var rand = sdir_mischarge[Math.floor(Math.random() * sdir_mischarge.length)];
    return rand*100; 
}