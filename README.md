# Samarkand #

Synthetic data generation package to run your data science experiments. 

And why the name? ***Samarqand*** is a city in south-eastern Uzbekistan and one of the oldest continuously inhabited cities in Central Asia. Alexander the Great conquered Samarkand in 329 BC. The city was known as Maracanda by the Greeks

## What is a synthetic dataset? ##
As the name suggests, quite obviously, a synthetic dataset is a repository of data that is generated programmatically. So, it is not collected by any real-life survey or experiment. Its main purpose, therefore, is to be flexible and rich enough to help an ML practitioner conduct fascinating experiments with various classification, regression, and clustering algorithms. Desired properties are,

* It can be numerical, binary, or categorical (ordinal or non-ordinal),
* The number of features and length of the dataset should be arbitrary
* It should preferably be random and the user should be able to choose a wide variety of statistical distribution to base this data upon i.e. the underlying random process can be precisely controlled and tuned,
* If it is used for classification algorithms, then the degree of class separation should be controllable to make the learning problem easy or hard,
* Random noise can be interjected, but only in a controllable manner

## What is wrong with random generation of data? ##
Randomly generated data may be fine for some purposes. But often data in the real world can have patterns. These patterns could be correlations between multiple features of the data. It is these correlations that is learned by the Machine Learning algorithm. 

A completely randomly generated dataset does not provide the machine learning algorithm to learn any useful insights. In some cases, it may just be trying to learn random noise. 

A subject matter expert might be able to comment on how the data features can be correlated and what patterns might exist. 

Data synthesis process would involve modelling these patterns and relationships, and then generating the data 

## How does this package enable data synthesis? ##

The package is written in object-oriented code. This makes it easier to model real-world scenarios. 

For example, a customer over 40 is likely to have a mortgage and a higher risk rating due to dependents. To model multiple relationship of this nature, it is difficult to encode these with if-else rules.  

### Installation instructions ###

* You need to clone the repository: 

        git clone git@bitbucket.org:rashed_karim/samarkand.git


* Dependencies 
** Python 
** Python packages (Pandas, Numpy, MatplotLib, Scikit-Learn, and many more)



### Contact ###

**Rashed Karim**
London
United Kingdom 
