''' 
@author: Rashed Karim,

Customer TimeSeries ML toolkit 

'''
import pandas as pd
import random
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans


import plotly.plotly as py
import plotly.graph_objs as go


#%% 
'''
    Detects anomalous charges, using a statistical outlier checked that is based 
    on a sliding window within time-series. The sliding window has a pre-defined 
    size that can be set, but defaults to 50 days, past and future 
'''
def ChargeOutliers(df_, col, n_std=3, window_size=50): 
    df = df_.copy()
    
    '''df['anomaly_factor'] = 0
    
    i = 0
    for index, row in df.iterrows():
        if i > window_size and i+window_size < len(df.index): 
             
    '''
    df['mean_fac'] = df[col].rolling(window_size).mean()
    df['std_fac'] = df[col].rolling(window_size).std()    
    df['anomalous'] = 0
    
    
    for index, row in df.iterrows():
        
        if np.isnan(row['mean_fac']) == False and np.isnan(row['std_fac']) == False: 
            if row[col] > row['mean_fac'] + (n_std*row['std_fac']): 
                df.at[index, 'anomalous'] = abs(row[col] - row['mean_fac'])/row['std_fac'] 
            else: 
                df.at[index, 'anomalous'] = 0
        
        
    return df


#%% 
'''
    Detects anomalous charges, using a statistical outlier checked that is based 
    on a sliding window within time-series. The sliding window has a pre-defined 
    size that can be set, but defaults to 50 days, past and future 
'''
def ChargeOutliers2(df_, col, n_std=3, window_size=50): 
    df = df_.copy()
    
    df['balance_normalise'] = 0
    for index, row in df.iterrows():
        if row['balance'] < 0: 
            df.at[index, 'balance_normalise'] = row[col] / -row['balance']
        
    
    '''df['anomaly_factor'] = 0
    
    i = 0
    for index, row in df.iterrows():
        if i > window_size and i+window_size < len(df.index): 
             
    '''
    df['mean_fac'] = df['balance_normalise'].rolling(window_size).mean()
    df['std_fac'] = df['balance_normalise'].rolling(window_size).std()    
    df['anomalous'] = 0
    df['n_std'] = 0
    
    
    for index, row in df.iterrows():
        
        if np.isnan(row['mean_fac']) == False and np.isnan(row['std_fac']) == False: 
            if row['std_fac'] > 0:
                if row[col] > row['mean_fac'] + (n_std*row['std_fac']): 
                    df.at[index, 'n_std'] = abs(row[col] - row['mean_fac'])/row['std_fac'] 
                    df.at[index, 'anomalous'] = 1
                else: 
                    df.at[index, 'n_std'] = abs(row[col] - row['mean_fac'])/row['std_fac'] 
                    df.at[index, 'anomalous'] = 0
            
        
    return df



'''
    Injects mischarges into a proportion (default 5%) of transactions 
    Expects df_ to be transactions for a single period within a certain period (1 year or etc.)
'''
#%%
def InjectSDIRMischarge(df_, sdir=0, proportion=0.05) : 
    
    df = df_.copy() 
    df = df.reset_index(drop=True)
    df['mischarge'] = 0         # new mischarge column to indicate what is injected 
    
    num_mischarges = round(proportion*len(df))
    
    # incorrect rates pool, randomly selected from this pool
    if sdir == 0:
        incorrect_rate_pool = [0.195,0.225,0.591,0.915] 
        sdir = random.choice(incorrect_rate_pool)
    
    index = list(df.index.values)
    
    if len(index) > num_mischarges:
        mischarge_indexes = random.sample(index, num_mischarges)
    else: 
        mischarge_indexes = index
    
    '''for i in mischarge_indexes: 
        balance = df.iloc[i]['balance']
        if balance < 0:
            df.at[i,'charge'] = sdir * -balance / 365
            df.at[i,'mischarge']= 1 
    '''
    
    # Also inject in 100% of customers on their informal overdraft 
    for index, row in df.iterrows():
        
        balance = df.iloc[index]['balance']
        if index in mischarge_indexes: 
            if balance < 0:
                df.at[index,'charge'] = sdir * -balance / 365
                df.at[index,'mischarge']= 1 
        
        # check if customer is on their informal overdraft, then they should be mischarged 
        if row['band3'] > 0: 
            df.at[index,'charge'] = sdir * -balance / 365
            df.at[index,'mischarge']= 1 
            
            
    return df 
        
        
    
#%% 
# Perform unsupervised cllustering  
def ClusterGenerator(write_to_file=0): 
    plt.figure(figsize=(15,10))
    
    
    # create normal clusters
    cluster_centers = [[25,25], [10, 6], [20, 10], [10,20]]
    X,y = make_blobs(n_samples=500, n_features=2, centers=cluster_centers, cluster_std=1.6, random_state=100)
    
    # create anomalous clusters 
    cluster_centers = [[20,18], [12, 7]]
    M,n = make_blobs(n_samples=50, n_features=2, centers=cluster_centers, cluster_std=0.5, random_state=100)
    
    # create more anomalous clusters 
    cluster_centers = [[25,3], [28, 15]]
    P,Q = make_blobs(n_samples=20, n_features=2, centers=cluster_centers, cluster_std=0.5, random_state=100)
  
    # Lone clusters
    cluster_centers = [[5,25], [5,15]]
    W,U = make_blobs(n_samples=2, n_features=2, centers=cluster_centers, cluster_std=0.5, random_state=100)
    
    L = np.concatenate((X, M))
    I  = np.concatenate((L, W))
    Z = np.concatenate((I, P))
    
    clf = KMeans(n_clusters=7,  random_state=0)
    clf.fit(Z)
    

    # create scatter plot
    axes = plt.scatter(Z[:,0], Z[:,1], c=clf.labels_, cmap='viridis')
    
    plt.xlim(0,30)
    plt.ylim(0,30)
    plt.xlabel('Size index')
    plt.ylabel('Overdraft volume index')
    
    if write_to_file != 0: 
        df = pd.DataFrame(data=Z, columns=['x','y'])
        df['label'] = clf.labels_
        
        df.to_csv(write_to_file)

#%% Test stub 
#roll = ChargeOutliers(new_cus_time_charge, 'rolling_mean')    

#
#df_ = InjectSDIRMischarge(this_customer, 0.195)

#%%
#anomalies = roll.loc[roll['anomalous'] > 0]

#%% 
ClusterGenerator('cluster_data.csv')