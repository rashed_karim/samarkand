# -*- coding: utf-8 -*-
"""
Created on Sun May 19 15:47:17 2019

@author: KV999XS
"""
from datetime import datetime
import random
import time
import calendar


def diff_month(d1, d2):
    return (d1.year - d2.year) * 12 + d1.month - d2.month


'''
https://stackoverflow.com/questions/553303/generate-a-random-date-between-two-other-dates
'''
def strTimeProp(start, end, format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format, time.localtime(ptime))


def randomDate(start, end, prop):
    return strTimeProp(start, end, '%d/%m/%Y %I:%M %p', prop)


''' 
Source: https://stackoverflow.com/questions/4130922/how-to-increment-datetime-by-custom-months-in-python-without-using-library
'''
def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year,month)[1])
    return datetime.date(year, month, day)


# Checks if a date falls between start and end 
# https://stackoverflow.com/questions/4695609/checking-date-against-date-range-in-python
def check_if_date_falls_between(sourcedate, start, end):
    return start <= sourcedate <= end