"""

@author: Rashed Karim,

Customer class represnt CMB customer

"""
import pandas as pd
import random
import numpy as np
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

'''
Synthesis of the following: 
    - Transaction period, start and end date 
    - In this period, for each year, synthesise annual interest rate within each band 
'''
class CustomerCMB2: 
    
        customer_id = -1
        trans_stdt = -1 
        trans_edt = -1 
        num_years = -1 
        rates = -1
        rates = []
        customer_segment = -1 
        num_customers = -1 
        seasonal_limit = -1 
        seasonal_index = -1 
        
        # constant band min, max rates for all CMB customers 
        # The interest rates listed here are x 10 
        bands = {
                1: [45, 125], 
                2: [45, 125], 
                3: [195,195]
                }
        
        # max over-draft limit in each customer segment 
        band_limit_range_segment = dict() 
        thousand = 10**3
        
        
        ''' 
            These constants determine two things:
                1) The overdraft limit in each band 
                2) The mode of the distribution from which overdraft is randomly sampled for this customer
        ''' 
        band_limit_range_segment['LGC'] = 20*thousand
        band_limit_range_segment['MMC'] = 10*thousand
        band_limit_range_segment['BBU'] = 5*thousand
        band_limit_range_segment['BBM'] = 5*thousand
        
        
        # seasonal pattern 
        seasonal_limit = dict()
        seasonal_limit['LGC'] = [30,50]
        seasonal_limit['MMC'] = [15,30]
        seasonal_limit['BBU'] = [7,10]
        seasonal_limit['BBM'] = [0,3]
        
        # default to 30,000, but the 30,000 is changed depending cust segment and sampled 
        # from a triangular moded distribution on band_limit_range_segment
        band_limit = [0, 30000]     
        
        def __init__(self, customer_id):           
            self.customer_id = customer_id 
            
           
        
        '''
            Set CMB customer segments LGC, MMC, BBU, BBM
            which also determines the overdraft limit size 
        '''
        def SetCustomerSegment(self, customer_segment): 
            self.customer_segment = customer_segment 
            
            # depending on customer segment, set mode of sampling
            # distribution to be centered on half of 
            mode = self.band_limit_range_segment[customer_segment]
            half_limit = mode / 2
            left = mode - half_limit 
            right = mode + half_limit 
            self.band_limit[1] = np.random.triangular(left, mode, right)
            
            # number of customers also depend on segment
            left = self.seasonal_limit[customer_segment][0]
            right = self.seasonal_limit[customer_segment][1]
            mode = (left+right)/2
            self.seasonal_index = np.random.triangular(left, mode, right)
        
        # start date is a random 1-8 years from start date 
        def SynTransStartEnd(self, num_years=0): 
            
            if num_years == 0: 
                self.num_years = random.choice([1,2,3,4,5,6,7,8])
            else:
                self.num_years = num_years
            
            trans_edt_str = '17-06-2019' 
            self.trans_edt = datetime.strptime(trans_edt_str, "%d-%m-%Y")

            self.trans_stdt = self.trans_edt - relativedelta(years=self.num_years)
            
        # Note SynTransStartEnd must be called prior to this 
        def SynAnnualRates(self): 
            
            if self.trans_edt != -1 and self.trans_stdt != -1:
                
                edt = self.trans_edt
                stdt = self.trans_stdt
                
                this_period_edt = stdt + relativedelta(years=1)
                
                band = self.SynEachBand()
                
                self.rates.append([stdt, this_period_edt, 1, band[1][0], band[1][1]])
                self.rates.append([stdt, this_period_edt, 2, band[2][0], band[2][1]])
                self.rates.append([stdt, this_period_edt, 3, band[3][0], band[3][1]])
                
                while this_period_edt < edt: 
                    
                    # update dates for next year, start and end 
                    stdt = this_period_edt + timedelta(days=1)
                    this_period_edt = stdt + relativedelta(years=1)
                    
                    # fetch newly agreed bands for next year 
                    band = self.SynEachBand()
                    self.rates.append([stdt, this_period_edt, 1, band[1][0], band[1][1]])
                    self.rates.append([stdt, this_period_edt, 2, band[2][0], band[2][1]])
                    self.rates.append([stdt, this_period_edt, 3, band[3][0], band[3][1]])
                    
        # Syntehsizes random interest rates, and overdraft limit for bands 1,2,3
        def SynEachBand(self): 
            
            band = dict() 
            
            w1 = random.uniform(0, 1); 
            w2 = 1 - w1
            
            for i in [1,2,3]:
                int_rate = random.randint(self.bands[i][0], self.bands[i][1])
                int_rate = int_rate/10 
                max_limit = self.band_limit[1]

                if i == 1:                     
                    band[i] = [int_rate, round(w1*max_limit, -3)]
                elif i == 2: 
                    band[i] = [int_rate, round(w2*max_limit, -3)]
                elif i==3: 
                    band[i] = [int_rate, 0]         # no limit under third band 
            return band
            
        def GetSeasonalIndex(self):
            return self.seasonal_index
            
#%% Test stub for class      
'''customer_cmb = CustomerCMB2(989888) 
customer_cmb.SetCustomerSegment('LGC')
customer_cmb.SynTransStartEnd(1)      
stdt = customer_cmb.trans_stdt 
customer_cmb.SynAnnualRates()

rates = customer_cmb.rates'''
        