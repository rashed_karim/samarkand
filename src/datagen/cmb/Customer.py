"""

@author: Rashed Karim,

Customer class represnt each customer that purchases Products 
and makes recurring installment payments towards this product

"""
import pandas as pd
import random
import Demography

class Customer: 
    
        customer_id = -1
        customer_type = -1 
        #payments = []  
        products = [] 
        demography = None 
        
        def __init__(self, customer_id):           
            self.customer_id = customer_id 
            self.cutomer_type = random.choice([1,2,3,4,5,6,7,8,9,10])
            
        
            
        def setCustomerType(self, customer_type): 
            self.customer_type = customer_type 
            
        '''def setPayment(self, payment): 
            self.payments.append(payment)
            
           def getPayments(self): 
            return self.payments  
            '''
            
        def setProducts(self, product):
            self.products.append(product)
            
        def setRandomizeDemography(self):
            random_demography = Demography.Demography()
            random_demography.setRandomizeDemography()
            self.demography = random_demography     

        def getProducts(self): 
            return self.products 
        
        def getProduct(self): 
            if len(self.products) > 0: 
                return self.products[0]
            else: 
                return -1
       
        
        '''
        dictionary structure, note there are also lists (e.g. payments)
            {
                'customer_id' : XXXX
                'product_list':(
                        'product_id' : XXXXXX
                        'term' : YY 
                        'product_type' : XX 
                        'principal' : XXXX
                        'balance' : XX 
                        'payments' : ( 
                                        'payment_date' : XX-XX-XXXX
                                        'expected_payment': XX
                                        'payment': XX 
                                     ), 
                                     ( .. ) 
                                }
                        
                        )
                
            }
        '''
        def getTransactions(self): 
            
            ret_dict = {}
            
            product_list = []
            for product in self.products :
                product_list.append(product.getProductDetails()) 
                
            ret_dict['product_list'] = product_list 
                
            ret_dict['customer_id'] = self.customer_id 
            
            ret_dict['customer_type'] = self.customer_type
            
            ret_dict['customer_age'] = self.age 
            
            return ret_dict
        
        
        def getCustomerPersonalInfo(self):
            
            ret_dict = self.demography.getDemographyAsDict() 
            ret_dict['customer_id'] = self.customer_id 
            ret_dict['customer_type'] = self.customer_type 
            