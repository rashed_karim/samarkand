"""

@author: Rashed Karim,

"""

#%% 
import pandas as pd
import numpy as np
import random 

import Customer
import Payment 
import Product 

#%% 
''' 
    Constants 
'''
size_population = 100000


# Read master table with variables, import into a dataframe
#%% 
data_model_file = "../data/model/master_table.xlsx"
sheet = "model"
df_variables = pd.read_excel(data_model_file, sheet, index_col = None)

#%% 
interest_rate_changes_archive = "../data/model/Interest_rate_changes_since_1694.xlsx" 
sheet = "INTEREST RATES" 
int_rates = pd.read_excel(interest_rate_changes_archive, sheet, index_col = None)


    
#%% 
product = customers[10].getProduct()

#%% 
v_ID = random.sample(range(1, 10**8), size_population) 


#%% 
df = pd.DataFrame(columns=df_variables['Attributes'], index=None)


#%% Cell
# Export to Excel
writer = pd.ExcelWriter('../data/simulated/nxtgen.xlsx')
df.to_excel(writer,'Sheet1')
df.to_csv('../data/simulated/nxtgen.csv')
writer.save()



