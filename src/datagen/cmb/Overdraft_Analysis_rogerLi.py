import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from numpy import random
from timeseries_ml import WindowedAnomalies

#%%
df = pd.read_csv('../../../data/EY_overdraft_charges_1000customers_1538_26062019.csv')


#%%
update_df = df.drop_duplicates('customer_id')
fig, axes = plt.subplots(nrows = 2, ncols = 3, figsize = (30, 15))

customer_ids = update_df.customer_id.unique()

a = 0
b = 0
customer_ids = random.choice (customer_ids, 20) 


style = dict(size = 10, color = 'gray')

for customer_id in customer_ids:
    if a < 2 and b < 3:
        cus_time_charge = df[['customer_id', 'charge', 'time']]
        update_df = cus_time_charge.loc[df['customer_id'] == customer_id] ## --> to locate these customers charges
        ## by far, there are 2922 * 3 dataframe sliced out from the original dataframe. all the left to do is to make sure
        ## rolling(100) is able to be calculated.
        df_ = update_df.loc[update_df['customer_id'] == customer_id]['charge']
        #print (update_df)
        ## the df_ is in consistent with cus_time_charge in terms of length --> 2922 rows.
        container = df_.rolling(300).mean()
        ## 2922 rows
        
        new_cus_time_charge = update_df.assign(rolling_mean = container)
        ## have put the series into the existing dataframe and need to remove all NaN

        new_cus_time_charge = new_cus_time_charge[np.isfinite(new_cus_time_charge['rolling_mean'])]
        ## NaN have been removed by far, end up with 2823 rows.
        #print (new_cus_time_charge)

        ##### to sort the whole dataframe by an English Date Format 
        new_cus_time_charge ['time'] = pd.to_datetime(new_cus_time_charge['time'], format = '%d-%m-%Y')
        sort_nctc = new_cus_time_charge.sort_values('time')
        #print (sort_nctc)
        ## get the whole dataframe sorted by time.

        max_min = sort_nctc['charge']
        
        
        fig.set_figheight(10)
        fig.set_figwidth(20)
        plt.subplots_adjust(hspace=0.6)
        sub_axis = sort_nctc.plot(x = 'time', y = 'rolling_mean', ax = axes[a,b], label='ID='+str(customer_id))
         
        df_with_anomaly = WindowedAnomalies(new_cus_time_charge, 'rolling_mean')
        #anomalies = df_with_anomaly.loc[df_with_anomaly['anomalous'] > 2]
        
        for idx, row in df_with_anomaly.iterrows():
            
            if df_with_anomaly.loc[idx, 'anomalous'] > 2:
                x = df_with_anomaly.loc[idx,'time']
                y = df_with_anomaly.loc[idx, 'rolling_mean']
                sub_axis.annotate('O', (x,y), xytext=(x,y), xycoords='data', textcoords='data', color='red')
    
        # axes[a, b].fill_between(sort_nctc, min(max_min), max(max_min),facecolor='yellow', alpha=0.3)
        # axes[a, b].set_autoscaley_on(False)
        # axes[a, b].set_ylim([0,max(max_min)*1.5])

        #print ("this is the container.index", container.index)
         ##--> the windows length is 100, and calcuate the mean.
        # ax: one parameter of pandas.DataFrame.plot()
        # TODO: get access to these 2 columns and apply for max() and min()
        #       ax.fill_between (x, min, max)
    if a < 2:
        a = a + 1
    else:
        a = 0
        b = b + 1
    
#%%
fig.savefig('Plots_1000customers_1701_24062019.png')