"""

@author: Rashed Karim,

"""

#%% 
import pandas as pd
import numpy as np
import random 

from sklearn.preprocessing import StandardScaler
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import CustomerCMB2 
from Overdraft import SpendingPattern

#%%
min_id = 1000000
max_id = 9999999
max_customers = 10
customers = 0
end_year=25
customer_id_list = [] 
customer_list = [] 
customer_transactions = [] 
customer_segment = ['LGC', 'MMC', 'BBU', 'BBM']
customer_seasonal_index = -1

#%%
# Generate customers with n=max_customers 
while customers < max_customers:
    customer_id = random.randint(min_id, max_id)
    customers = customers+1
    if customer_id not in customer_id_list:
        customer_id_list.append(customer_id)
        customer_cmb = CustomerCMB2.CustomerCMB2(customer_id)
        this_customer_segment = random.choice(customer_segment)
        customer_cmb.SetCustomerSegment(this_customer_segment)    # random customer segment assignment      
        customer_seasonal_index = customer_cmb.GetSeasonalIndex()
        
        # randomise start and end transaction period 
        num_years = 1
        customer_cmb.SynTransStartEnd(num_years)            # generate transactions for num_years = 1      
        customer_cmb.SynAnnualRates()
        
        
        transactions = SpendingPattern(customer_cmb)
        transactions.GenerateTransaction() 
        
        all_transactions = transactions.transactions
        
        for key, transactions in all_transactions.items(): 
            
            trans_date = key.strftime("%d-%m-%Y")
            balance = transactions[0]
            
            band1_charge = transactions[1]
            band2_charge = transactions[2] 
            band3_charge = transactions[3]
            
            band1_rate = transactions[4]
            band2_rate = transactions[5]
            
            band1_limit = transactions[6]
            band2_limit = transactions[7]
            
            total_charge = transactions[8] 
            
            customer_transactions.append([customer_id, this_customer_segment, customer_seasonal_index, trans_date, balance, band1_charge, band2_charge, band3_charge, band1_rate, band2_rate, band1_limit, band2_limit, total_charge])    

#%%
customer_transactions_df = pd.DataFrame(data=customer_transactions, columns=['customer_id','customer_segment', 'seasonal_index', 'time', 'balance','band1','band2','band3','band1_rate','band2_rate','band1_limit','band2_limit','charge'])

#%% 
customer_transactions_df.to_csv('time_series.csv')