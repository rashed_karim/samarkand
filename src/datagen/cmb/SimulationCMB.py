"""

@author: Rashed Karim,

"""

#%% 
import pandas as pd
import numpy as np
import random 

from sklearn.preprocessing import StandardScaler
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import CustomerCmb

#%%
min_id = 1000000
max_id = 9999999
max_customers = 10000
customers = 0
end_year=25
customer_id_list = [] 
customer_list = [] 


#%%
# Generate customers with n=max_customers 
while customers < max_customers:
    customer_id = random.randint(min_id, max_id)
    if customer_id not in customer_id_list:
        customer_id_list.append(customer_id)
        customer_cmb = CustomerCmb.CustomerCMB(customer_id)
        customer_cmb.setCustomerType(random.choice([1,2,3,4,5,6,7,8,9]))
        customer_cmb.setCustomerValue()
        customer_cmb.setCustomerSize()
        customer_list.append(customer_cmb)
        customers = customers+1 




#%% 
# Write customers to a dataframe 
customer_list2 = [] 
for customer in customer_list: 
    this_customer = [customer.customer_id, customer.customer_type, customer.customer_size, customer.customer_value]
    customer_list2.append(this_customer) 
customers_df = pd.DataFrame(data=customer_list2, columns = ['id','type','size','value'])


#%%
# Z-score scaling  
ss = StandardScaler()
customers_df_scaled = pd.DataFrame(ss.fit_transform(customers_df),columns = customers_df.columns)

#%% 
# Plot in 3D  
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = customers_df['type']
y = customers_df['size']
z = customers_df['value']
ax.scatter(x,y,z, c='r', marker='o')

#%%
# Plot in 2D 
customers_df.plot.scatter(x='size', y='value')


#%%
customers_df.to_csv('../../../data/customercmb.csv')

#%% 
#df = pd.DataFrame(columns=['customer_id','customer_type','product_id','principal','expected_payment','payment','balance','date','term'])
df_rows = []


customer_id = transactions_C['customer_id']
customer_type = transactions_C['customer_type']
customer_age = transactions_C['customer_age']
product_list = transactions_C['product_list']
 
for product in product_list:
    
    # about the product
    product_id = product['product_id']
    
    product_principal = product['principal']
    
    opening_balance = product_principal
    prev_balance = opening_balance
    
    product_term = product['term']
    product_start_date = product['start_date']
    
    payments = product['payments']
    
    # the payments made towards this product
    for payment in payments: 
        
        payment_date = payment['payment_date']
        expected_payment = payment['expected_payment']
        actual_payment = payment['payment']
        
        row_dict = {} 
        row_dict['customer_id'] = customer_id 
        row_dict['customer_type'] = customer_type 
        row_dict['customer_age'] = customer_age
        row_dict['product_id'] = product_id 
        row_dict['principal'] = product_principal 
        row_dict['expected_payment'] = expected_payment 
        row_dict['payment'] = actual_payment 
        row_dict['balance'] = prev_balance - actual_payment 
        row_dict['date'] = payment_date 
        row_dict['term'] = product_term 
        
        prev_balance = row_dict['balance']
        
        df_rows.append(row_dict)

df = pd.DataFrame(df_rows, columns=['customer_id','customer_type','customer_age','product_id','principal','start_date','expected_payment','payment','balance','date','term'])
df.to_csv('../data/simulated/simulation.csv')
#%%

for i in range(0,max_customers): 
    customer_id = random.randint(min_id, max_id)
    product_id = random.randint(min_id, max_id)
    
    customer = Customer.Customer(customer_id)
    mortgage = Product.Product(product_id)
    
    customer.setProducts(mortgage)    
    customers.append(customer)