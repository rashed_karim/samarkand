"""

@author: Rashed Karim,

Implements Various Seasonal patterns for Time Series
Sources of cyclic seasonal data: 
https://raw.githubusercontent.com/jbrownlee/Datasets/master/monthly-sunspots.csv
https://raw.githubusercontent.com/jbrownlee/Datasets/master/daily-total-female-births.csv
https://raw.githubusercontent.com/jbrownlee/Datasets/master/daily-min-temperatures.csv

"""
#%%
import random
import pandas as pd
import numpy as np
from matplotlib import pyplot


from util import randomDate
from util import add_months
from util import check_if_date_falls_between
from datetime import datetime
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta

class SeasonalCycle: 
    
    cycle_amplitude = -1 
    cycle_frquency = -1 
    raw_data = -1 
    time_format = -1 
    
    
    def parser(self,x):
        return datetime.strptime(''+x, '%Y-%m')
    
    
    def __init__(self, choose_data): 
        
        if choose_data == 1: 
            self.raw_data = pd.read_csv('./data/daily-min-temperatures.csv')
            self.time_format = '%d-%m-%Y'
            
        elif choose_data == 2: 
            self.raw_data = pd.read_csv('./data/daily-total-female-births.csv')
            self.time_format = '%d/%m/%Y'
           
        else: 
            self.raw_data = pd.read_csv('./data/monthly-sunspots.csv')
            self.raw_data['Month'] = self.raw_data['Month'] + '-01'
            self.time_format = '%Y-%m-%d'
           
            
        self.raw_data.columns = ['time','value']
        for i, row in self.raw_data.iterrows():
            time = row['time']
            row['time'] = datetime.strptime(time,self.time_format)
        
        
    
    
        
        
#%%     
seasonal = SeasonalCycle(3)
raw_data = seasonal.raw_data 
upsampled = raw_data.resample('365D')
interpolated = upsampled.interpolate(method='spline', order=2)