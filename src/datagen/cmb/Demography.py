# -*- coding: utf-8 -*-
"""
Created on Mon May 20 14:07:58 2019

@author: KV999XS
"""

import pandas as pd
import random

class Demography: 
    
    age = -1 
    sex = -1
    race = -1 
    household_income = -1 
    home_ownership = -1 
    education = -1 
    children = -1 
    
    def __init__(self):           
        self.age = 20 
        self.sex = 1
        self.houshold_income = 20000 
        self.education = 1
        self.children = 0
        
    
    def setRandomizeDemography(self): 
        self.age = random.randint(25,55)
        self.sex = random.choice([0,1])
        self.race = random.choice([1,2,3,4,5])
        self.household_income = random.randint(20000,200000)
        self.home_ownership = random.choice([0,1])
        self.education = random.choice([1,2,3,4])
        self.children = random.choice([0,1,2,3,4,5])
        
    
    def getDemographyAsDict(self): 
        
        ret_dict = {}         
        ret_dict['age'] = self.age             
        ret_dict['sex'] = self.sex         
        ret_dict['race'] = self.race        
        ret_dict['household_income'] = self.household_income 
        ret_dict['home_ownership'] = self.home_ownership
        ret_dict['education']= self.education
        ret_dict['children'] = self.children 
        
        return ret_dict 