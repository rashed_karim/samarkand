"""

@author: Rashed Karim,

Payment class represents recurring payments made towards a product

"""

class Payment: 
    
        payment_id = -1 
        customer_id = -1 
        
        amount = -1
        #product = -1 
        
        payment_date = -1
        expected_payment = -1
        
        
        def __init__(self, payment_id):           
            self.payment_id = payment_id 
           

        def setAmount(self, amount): 
            self.amount = amount  
                 
        '''def setProduct(self, product): 
            self.product = product '''
        
        def setCustomer(self, customer_id): 
            self.customer_id = customer_id 
            
        def setPaymentDate(self, which_date):
            self.payment_date = which_date
        
        def setExpectedPayment(self, expected_payment): 
            self.expected_payment = expected_payment 
                        
        def getPaymentAmount(self) :
            return self.amount
        
        def getPaymentDate(self): 
            return self.payment_date
        
        def getExpectedPayment(self): 
            return self.expected_payment
        
        
        




                    
