"""

@author: Rashed Karim,

Customer class represnt CMB customer

"""
import pandas as pd
import random


class CustomerCMB: 
    
        customer_id = -1
        customer_type = -1 
        customer_value = -1 
        customer_size = -1 
        customer_min_rate = -1   # customer rate band 
        customer_max_rate = -1 
        
        def __init__(self, customer_id):           
            self.customer_id = customer_id 
            
        def setCustomerType(self, which_type): 
            self.customer_type = which_type
            
        '''
            The type of corporation determines value
            1,2,3 - 50+ million value 
            4,5,6 - 10+ million value 
            7,8,9 - 1+ million value 
        '''
        
        def setCustomerValue(self): 
            million = 10**6
            cmb_value = {
                1 : [75*million, 100*million], 
                2 : [50*million, 75*million],            
                3 : [25*million, 50*million], 
                4 : [15*million, 25*million],
                5 : [10*million, 15*million],
                6 : [5*million, 10*million],
                7 : [3*million, 5*million],
                8 : [1*million, 3*million],
                9 : [0.01*million, 1*million]
            }
            '''
            if self.customer_type == 1: 
                self.customer_value = random.randint(75*million, 100*million)
            else if self.customer_type == 2: 
                self.customer_value = random.randint(50*million, 75*million)
            else if self.customer_type == 3: 
                self.customer_value = random.randint(25*million, 50*million)
            else if self.customer_type == 4: 
                self.customer_value = random.randint(15*million, 25*million)
            else if self.customer_type == 5: 
                self.customer_value = random.randint(10*million, 15*million)
            else if self.customer_type == 6: 
                self.customer_value = random.randint(5*million, 10*million)
            else if self.customer_type == 7: 
                self.customer_value = random.randint(3*million, 5*million)
            else if self.customer_type == 8: 
                self.customer_value = random.randint(1*million, 3*million)
            else if self.customer_type == 9: 
                self.customer_value = random.randint(0.01*million, 1*million)
                '''
            cmb_type = self.customer_type 
            
            min_value = cmb_value[cmb_type][0]
            max_value = cmb_value[cmb_type][1] 
            
            self.customer_value = random.randint(min_value, max_value)
        
        '''
             value determines the size of the company 
             But, also type is determined by value 
             
        '''
        def setCustomerSize(self): 
            thousand = 10**3 
            hundred = 10**2
            
            '''
                For each CMB type, there is a size range 
            '''
            cmb_sizes = {
                    1 : [75*thousand, 100*thousand], 
                    2 : [50*thousand, 75*thousand],            
                    3 : [10*thousand, 50*thousand], 
                    4 : [7*thousand, 10*thousand],
                    5 : [3*thousand, 7*thousand],
                    6 : [1*thousand, 3*thousand],
                    7 : [0.7*thousand, 1*thousand],
                    8 : [0.5*thousand, 0.7*thousand],
                    9 : [1*hundred, 0.5*thousand]
                    }
            
            cmb_type = self.customer_type 
            
            min_size = cmb_sizes[cmb_type][0]
            max_size = cmb_sizes[cmb_type][1] 
            
            self.customer_size = random.randint(min_size, max_size)
            
        
            
        