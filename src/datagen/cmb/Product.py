"""

@author: Rashed Karim,

Product class represents a loan, for example a mortgage

"""
import random
import Payment 
from util import diff_month
from datetime import datetime
from dateutil.relativedelta import relativedelta

max_product_types = 10
max_amount = 1000000
min_amount = 100000
terms = [25,30,35,40,45]

class Product: 
        
        product_id = -1 
        #ustomer_id = -1
        principal_amount = -1
        product_type = -1
        interest_rates = {} 
        balance = 0 
        risk = 0
        payments = [] 
        term_years = 0 
        start_date = '1-1-2007'
        
       # mortgage_id = -1 
        
        
        
        def __init__(self, product_id):
            self.product_id = product_id 
            
            
            
        def setRandomProduct(self):
            self.product_type = random.randint(1,max_product_types)
            self.setPrincipalAmount(random.randint(min_amount,max_amount))
            self.setTerm(random.choice(terms))
            

        def setPrincipalAmount(self, principal_amount): 
            self.principal_amount = principal_amount  
            self.balance = principal_amount
            
        def setInterestRate(self, time, rate): 
            self.interest_rates[time] = rate 
        
        def setProductType(self, product_type): 
            self.product_type = product_type
            
        def setPayment(self, payment): 
            self.payments.append(payment)
            if self.balance - payment.amount >= 0:  
                self.balance = self.balance - payment.amount 
        
        def setRisk(self, risk):    
            self.risk = risk 
            
        def setTerm(self, term_years):
            self.term_years = term_years
            
            
        def getProductID(self): 
            return self.product_id
        
        '''
            Warning: the calculation of payment installments 
            doesn't take interest into account
        '''
        def setRandomizePayments(self, start_date, end_date): 
            
            
            start_date = datetime.strptime(start_date, '%d-%m-%Y')
            self.start_date = start_date 
            
            end_date = datetime.strptime(end_date, '%d-%m-%Y')
            p = self.principal_amount 
            t = self.term_years
            
            months= diff_month(end_date, start_date)
            
            if p>0 and t>0:     
                monthly = p / (t*12)    
                
                count_month = 0 
                for month in range(0, months):
                    payment_id = random.randint(100000,999999)
                    payment = Payment.Payment(payment_id)
                    
                    payment.setAmount(monthly) 
                    payment.setExpectedPayment(monthly)
                    
                    payment_date = start_date + relativedelta(months=+count_month)
                    payment.setPaymentDate(payment_date)
                    
                    self.setPayment(payment)
                    count_month = count_month+1
                    
                    if count_month % 12 == 0 and count_month > 0 and self.term_years - 1 > 1:
                       self.term_years = self.term_years - 1
                    
                
        def getPayments(self):
            return self.payments
                
                
        def getTerm(self): 
            return self.term_years
        
        def getPrincipalAmount(self): 
            return self.principal_amount
        
        def getBalnace(self): 
            return self.balance
        
        
        def getProductDetails(self): 
            
            product_items = {} 
            payments = self.payments
            payments_list = [] 
                
            '''
                List all payments for thsi product 
            '''
            for payment in payments: 
                payment_dict = {} 
                payment_dict['payment_date'] = payment.getPaymentDate()
                payment_dict['expected_payment'] = payment.getExpectedPayment() 
                payment_dict['payment'] = payment.getPaymentAmount()
                
                payments_list.append(payment_dict)
            
            product_items['payments'] = payments_list 
            
            product_items['product_id'] = self.product_id 
            product_items['term'] = self.term_years
            
            product_items['principal'] = self.principal_amount
            product_items['balance'] = self.balance 
            
            product_items['product_type'] = self.product_type 
            product_items['start_date'] = self.start_date
            
            return product_items
        




                    
