"""

@author: Rashed Karim,

Overdraft class represents a balance overdraft
For more info: https://www.business.hsbc.uk/en-gb/interest-rates/interest-rates-finance-borrowing

"""
import random
import pandas as pd
import numpy as np

from CustomerCmb2 import CustomerCMB2

from util import randomDate
from util import add_months
from util import check_if_date_falls_between
from datetime import datetime
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta

class SpendingPattern: 
    
        mean_spend = -1 
        std_spend = -1 
        customer_cmb = -1  
        customer_segment = -1
        
        transactions = dict()           # keys is each day, and value is end of day balance each day with charge amounts in each band 
        # some constants  
        customer_spend_limit = [0,30000] 
    
    
        def __init__(self, customer_cmb):
            self.customer_cmb = customer_cmb 
            self.customer_segment = customer_cmb.customer_segment
        
        
        def setPeriod(self, period):
            self.total_period = total_period
    
    
        def GenerateTransaction(self): 
            
            # Range for daily spend and start balance 
            # Note - this is not varying for customer segments 
            daily_balance_mode = 0
            daily_balance_range = [-1000, 1000]
            start_balance_range = [-1000, 1000]
            
            trans_stdt = self.customer_cmb.trans_stdt
            trans_edt = self.customer_cmb.trans_edt 
            
            #mu=random.randint(-30000,5000)
            #sigma=random.randint(100,5000) 
            
            trans_dt = trans_stdt
            #trans_dt = add_months(trans_stdt, 1)
            #trans_dt = trans_dt.replace(day=1)               # overdraft charges are first day of month 
            band_charge = dict() 
            
            # lets start with a balance 
            balance = random.randint(start_balance_range[0], start_balance_range[1])
            balance = round(balance, -2)

            # Each day in simulation this is what happens:
            while trans_dt < trans_edt: 
    
                '''
                    Sampling their balance from a triangular distribution 
                    that is centered around their typical spending amount
                '''
                daily_spend_min = daily_balance_mode + daily_balance_range[0]
                daily_spend_max = daily_balance_mode + daily_balance_range[1]
                
                typical_spend = range(daily_spend_min, daily_spend_max, 10)
                today_spend = random.choice(typical_spend)
                balance = balance + today_spend
            
                
                band_charge = self.DetermineCharge(balance, trans_dt)
                
                band_charge[1] = round(band_charge[1], 2)
                band_charge[2] = round(band_charge[2], 2)
                band_charge[3] = round(band_charge[3], 2)
                
                total_charge = band_charge[1] + band_charge[2] + band_charge[3]
    
                self.transactions[trans_dt] = [balance, band_charge[1], band_charge[2], band_charge[3], band_charge[4], band_charge[5], band_charge[6], band_charge[7], round(total_charge,2)]
                trans_dt = trans_dt + timedelta(days=1)   # move to the next day 
                
      
      
        def DetermineCharge(self, balance, which_date): 
           
           band_charge = dict() 
           
           band_charge[1] = 0 
           band_charge[2] = 0 
           band_charge[3] = 0 
           band_charge[4] = 0
           band_charge[5] = 0
           band_charge[6] = 0
           band_charge[7] = 0
           
           if balance > 0: 
               return band_charge               # not overdrawn - hence no charge
           else: 
                band_charge[1] = -1             # to be determined below 
                band_charge[2] = -1 
                band_charge[3] = -1  
                band_charge[4] = -1
                band_charge[5] = -1
                band_charge[6] = -1
                band_charge[7] = -1
                balance = abs(balance)          # charges are positive 
        
           for idx, annual_periods in enumerate(self.customer_cmb.rates):
               
               # refer to Customer_CMB2 class rates variable for contents 
               start_of_period = annual_periods[0]
               end_of_period = annual_periods[1]
               band1_rate = annual_periods[3]
               band1_limit = annual_periods[4]
               
               band2_rate = self.customer_cmb.rates[idx+1][3]      # the next record in array has band2 
               band2_limit = self.customer_cmb.rates[idx+1][4]      # the next record in array has band2 
               
               band3_rate = self.customer_cmb.rates[idx+2][3] 
               band3_limit = -1         # informal limit has no limit
               
               if check_if_date_falls_between(which_date, start_of_period, end_of_period): 
                   
                   total_band12 = band1_limit + band2_limit
                   
                   if balance < total_band12: 
                       
                        if balance < band1_limit: 
                           
                            band_charge[1] = balance * band1_rate/(365*100)
                            band_charge[2] = 0
                            band_charge[3] = 0
                            
                       
                        else:
                            residual1 = balance - band1_limit 
                       
                            band_charge[1] = balance * band1_rate/(365*100)
                            band_charge[2] = residual1 * band2_rate/(365*100)
                            band_charge[3] = 0
                            
                       
                            
                   else: 
                        residual1 = balance - band1_limit 
                        residual2 = residual1 - band2_limit 
                           
                        band_charge[1] = band1_limit * band1_rate/(365*100)
                        band_charge[2] = residual1 * band2_rate/(365*100)
                        band_charge[3] = residual2 * band3_rate/(365*100)
                   
                   band_charge[4] = band1_rate
                   band_charge[5] = band2_rate
                   band_charge[6] = band1_limit    # useful to know limits 
                   band_charge[7] = band2_limit
                   
               
                   break
                 
           return band_charge 
            
            
#%% Test stub for class      
customer_cmb = CustomerCmb2.CustomerCMB2(989888) 
customer_cmb.SetCustomerSegment('BBM')
customer_cmb.SynTransStartEnd()      

customer_cmb.SynAnnualRates()

rates = customer_cmb.rates

transactions = SpendingPattern(customer_cmb)
transactions.GenerateTransaction() 

all_t = transactions.transactions