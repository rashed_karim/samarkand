'''
    ML testing and validation for SDIR mischarges with 
    results of visualisations on time-series plots 
    
    @author: Rashed Karim 
    @organisation: EY Advisory 
    
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from numpy import random
from timeseries_ml import ChargeOutliers, ChargeOutliers2, InjectSDIRMischarge

#%% Load the annual transaction simluation dataset 
# Refer to SimulationOverdraft for simulated dataset generation. There are pre-built simulation 
# datasets that should be used. These can be found /data/ folder 
df = pd.read_csv('../../../data/Ey_overdraft_charges_1000customers_1807_26062019.csv')


#%% Extract unique customers from this entire transaction simulation dataset
update_df = df.drop_duplicates('customer_id')
fig, axes = plt.subplots(nrows = 2, ncols = 3, figsize = (30, 15))

# Preparing for ML testing for a random set of these customers 
# ML testing involves plotting, so select a random set, and randomly plot a subset of these 
random_set = 10     # customers 
customer_ids = update_df.customer_id.unique()
customer_ids = random.choice (customer_ids, random_set) 

# Some plot parameters
a = 0
b = 0
style = dict(size = 10, color = 'gray')


# Some parameters worth checking before running simulation 
sliding_window_size = 20            # the number of days before and after current day to perform statistical testing on 
                                    # for checking mischarge anomalies 

sdir = 0                        # The mischarge rate for injecting anomalous charges, 
                                    # if this is set to 0, it selects from a pool of mischarge rates, refer to timeseries_ml file 


# Run ML testing and validation by plotting and ML mischarge check on a*b customers 
for customer_id in customer_ids:
    if a < 2 and b < 3:
        cus_time_charge = df[['customer_id', 'balance', 'band3', 'charge', 'time']]
        this_customer = cus_time_charge.loc[df['customer_id'] == customer_id] ## --> to locate these customers charges
        ## by far, there are 2922 * 3 dataframe sliced out from the original dataframe. all the left to do is to make sure
        ## rolling(100) is able to be calculated.
        
        
        ##### to sort the whole dataframe by an English Date Format 
        this_customer ['time'] = pd.to_datetime(this_customer['time'], format = '%d-%m-%Y')
        this_customer_sorted = this_customer.sort_values('time')
        #print (sort_nctc)
        ## get the whole dataframe sorted by time.

        max_min = this_customer_sorted['charge']
        
        
        fig.set_figheight(10)
        fig.set_figwidth(20)
        plt.subplots_adjust(hspace=0.6)
        sub_axis = this_customer_sorted.plot(x = 'time', y = 'charge', ax = axes[a,b], label='ID='+str(customer_id))
        
        # inject some SDIR mischarges into this customr's transactions 
        # random choice of the proportion (1-5 %) of mischarges 
        
        #proportions = [0.01, 0.02, 0.03, 0.04, 0.05]
        #mischarge_proportion = random.choice(proportions)

        this_customer_mischarge = InjectSDIRMischarge(this_customer_sorted, sdir)
        
        
        df_with_anomaly = ChargeOutliers2(this_customer_mischarge, 'charge', 3, sliding_window_size)
        
        #anomalies = df_with_anomaly.loc[df_with_anomaly['anomalous'] > 2]
        
        for idx, row in df_with_anomaly.iterrows():
            
            if df_with_anomaly.loc[idx, 'anomalous'] > 3:
                x = df_with_anomaly.loc[idx,'time']
                y = df_with_anomaly.loc[idx, 'charge']
                
                sub_axis.annotate('O', (x,y), xytext=(x,y), xycoords='data', textcoords='data', color='red')
            
            if df_with_anomaly.loc[idx, 'mischarge'] > 0:
                x = df_with_anomaly.loc[idx,'time']
                y = df_with_anomaly.loc[idx, 'charge']
                
                sub_axis.annotate('O', (x,y), xytext=(x,y), xycoords='data', textcoords='data', color='blue')
    
       
    if a < 2:
        a = a + 1
    else:
        a = 0
        b = b + 1
    
#%%
fig.savefig('Plots_1000customers_1701_24062019.png')



#%% 
# customer_id = 8156714
customer_id = 2902682       # this customer goes over their informal overdraft limit 
cus_time_charge = df[['customer_id', 'balance','band3', 'charge','time']]
this_customer = cus_time_charge.loc[df['customer_id'] == customer_id] ## --> to locate these customers charges

## by far, there are 2922 * 3 dataframe sliced out from the original dataframe. all the left to do is to make sure
## rolling(100) is able to be calculated.


##### to sort the whole dataframe by an English Date Format 
this_customer ['time'] = pd.to_datetime(this_customer['time'], format = '%d-%m-%Y')
this_customer_sorted = this_customer.sort_values('time')
#print (sort_nctc)
## get the whole dataframe sorted by time.

#%% write as a text file
charges_as_txt=  "" 
for index, row in this_customer_sorted.iterrows(): 
    charges_as_txt = charges_as_txt + str(row['charge']) + ","

f = open("charges.txt", "a")
f.write(charges_as_txt)
f.close() 

#%% Write as dataframe 
write_df = this_customer[['time','band3','charge']]
write_df.to_csv('charges_2902682.csv')



#%% 
# Write balance as dataframe 
write_df = this_customer[['time','balance']]
write_df.to_csv('balance_2902682.csv')