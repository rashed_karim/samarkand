"""

@author: Rashed Karim,

"""

#%% 
import pandas as pd
import numpy as np
import random 

import Customer
import Payment 
import Product 
import Demography 

#%%
min_id = 1000000
max_id = 9999999
max_customers = 100000
customers = []
end_year=25

#%% 
customer_id = random.randint(min_id, max_id)
product_id = random.randint(min_id, max_id)
customer_C = None
customer_C = Customer.Customer(customer_id)
mortgage_M = Product.Product(product_id)
mortgage_M.setRandomProduct()
customer_C.setProducts(mortgage_M)

mortgage_M.setRandomizePayments('1-1-2007','1-1-2019')

#%% 
transactions_C = customer_C.getTransactions()


#%%
# test demography
for i in (0,10000):
    customer_id = random.randint(min_id, max_id)
    customer = Customer.Customer(customer_id)


#%% 
#df = pd.DataFrame(columns=['customer_id','customer_type','product_id','principal','expected_payment','payment','balance','date','term'])
df_rows = []


customer_id = transactions_C['customer_id']
customer_type = transactions_C['customer_type']
customer_age = transactions_C['customer_age']
product_list = transactions_C['product_list']
 
for product in product_list:
    
    # about the product
    product_id = product['product_id']
    
    product_principal = product['principal']
    
    opening_balance = product_principal
    prev_balance = opening_balance
    
    product_term = product['term']
    product_start_date = product['start_date']
    
    payments = product['payments']
    
    # the payments made towards this product
    for payment in payments: 
        
        payment_date = payment['payment_date']
        expected_payment = payment['expected_payment']
        actual_payment = payment['payment']
        
        row_dict = {} 
        row_dict['customer_id'] = customer_id 
        row_dict['customer_type'] = customer_type 
        row_dict['customer_age'] = customer_age
        row_dict['product_id'] = product_id 
        row_dict['principal'] = product_principal 
        row_dict['expected_payment'] = expected_payment 
        row_dict['payment'] = actual_payment 
        row_dict['balance'] = prev_balance - actual_payment 
        row_dict['date'] = payment_date 
        row_dict['term'] = product_term 
        
        prev_balance = row_dict['balance']
        
        df_rows.append(row_dict)

df = pd.DataFrame(df_rows, columns=['customer_id','customer_type','customer_age','product_id','principal','start_date','expected_payment','payment','balance','date','term'])
df.to_csv('../data/simulated/simulation.csv')
#%%

for i in range(0,max_customers): 
    customer_id = random.randint(min_id, max_id)
    product_id = random.randint(min_id, max_id)
    
    customer = Customer.Customer(customer_id)
    mortgage = Product.Product(product_id)
    
    customer.setProducts(mortgage)    
    customers.append(customer)